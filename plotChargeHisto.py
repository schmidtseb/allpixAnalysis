#!/usr/bin/env python
import ROOT
import numpy as np
import matplotlib.pyplot as plt
import scipy.stats
import cPickle

INFILE = 'Am241/Am241_out.root'
SCALE = 3.62 / 1000     # Transform into keV
THL = 10                # in keV
THH = 65
BINS = 100

def main():
    listCharge = importSpectrumROOT(INFILE, 'charge')
    binsCharge, dataCharge = getHistogram(listCharge, THL, THH, BINS)

    chargeSmearList = smearSpectrumFunction(listCharge, [2.49, 0.09, 1.06])
    binsChargeSmear, dataChargeSmear = getHistogram(chargeSmearList, THL, THH, BINS) 

    plotHistogram(binsCharge, dataCharge)
    plotHistogram(binsChargeSmear, dataChargeSmear)
    plt.show()

def importSpectrumROOT(fName, key='charge'):
    f = ROOT.TFile.Open(fName, 'READ')
    t = f.Get('clusters')

    N = t.GetEntries()

    # Get signal and charge entries from ROOT-file
    dataList = []
    for i in range(N):
        t.GetEntry(i)

        if key=='totalCharge':
            elm = t.totalCharge # t.charge
            dataList.append( elm )
        else:
            if key == 'charge':
                elm = t.charge
            elif key == 'signal':
                elm = t.signal

            if elm.size():
                # print np.asarray( t.charge )
                # signalList.append( np.asarray( t.signal )[0] )
                dataList += list( elm )

    f.Close()

    # Transform charge into energy
    dataList = np.asarray( dataList ) * SCALE

    return dataList

def getHistogram(dataList, THL, THH, bins, density=True):
    dataList = np.asarray( dataList )
    data, bins = np.histogram(dataList[np.logical_and(dataList >= THL, dataList <= THH)], bins=bins, density=False)
    if density:
        return bins, data / float(np.sum(data))
    else:
        return bins, data

def smearSpectrumConstant(data, sigma):
    data = np.asarray( list(data) * 10 )
    data_smear = np.random.normal(np.zeros(len(data)), scale=sigma)
    # print data_smear

    return data + data_smear

def smearSpectrumFunction(data, sigmaParams, func='exp'):
    data = np.asarray( list(data) * 10 ) 

    def expDecay(x, A, k, c):
        return A*np.exp(-k*x) + c

    def hyperbolic(x, A, k, c):
        return A/(k*x**c +1) - A

    if func=='exp':
        f = expDecay
    elif func =='hyper':
        f = hyperbolic

    sigma = np.abs(f(data, *sigmaParams))
    data_smear = np.random.normal(np.zeros(len(data)), scale=sigma)

    return data + data_smear

def smearSpectrumKDE(data, sigma, THL, THH, bins):
    data = data[np.logical_and(data >= THL, data <= THH)]

    x = np.linspace(min(data), max(data), bins)
    y = np.sum(np.asarray([scipy.stats.norm.pdf(x - mu, scale=sigma) for mu in data]), axis=0)

    return x, y

# = PLOT =
def plotHistogram(bins, data, xlabel='Energy (keV)', ylabel='Probability', log=False):
    fig, ax = plt.subplots()
    ax.step(bins[:-1], data, where='post')

    # Plot properties
    if log:
        ax.set_yscale("log", nonposy='clip')
    plt.grid(True, which="both")
    ax.set_xlabel( xlabel )
    ax.set_ylabel( ylabel )

    plt.tight_layout()

if __name__ == '__main__':
    main()

